# Arcadia Application

The following repository contains application and infrastructure deployment  
configuration for an example microservices banking application.

Each folder is a submodule with their own repository.

- [Application Frontend](https://github.com/apcj-f5/arcadia-frontend/)
- [Application Backend](https://github.com/apcj-f5/arcadia-backend/)
- [Money Transfer Application](https://github.com/apcj-f5/arcadia-app2/)
- [Refer A Friend Application](https://github.com/apcj-f5/arcadia-app3/)

- [Configuration for NGINX Ingress Controller, API Gateway and App Protect](https://github.com/apcj-f5/arcadia-nginx/)
- [Configuration for deployment into a Kubernetes cluster](https://github.com/apcj-f5/arcadia-kubernetes/)

**Maintainer:** ![@shsingh](https://avatars.githubusercontent.com/u/412800?s=15&v=4)

<br>
<br>

Modified from upstream project: [arcadia-application](https://gitlab.com/arcadia-application)
